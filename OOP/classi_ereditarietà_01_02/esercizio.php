<?php

class Zoo {
  //Attributi
  public $famiglia;
  public $specie;
  public $tipo;

  public function __construct($family, $species, $type) {
    $this -> famiglia = $family;
    $this -> specie = $species;
    $this -> tipo = $type;
  }

  public function saluto(){
    echo "Io sono il $this->tipo, benvenuti!\n";
  }
}

$animale_1 = new Zoo("mammiferi", "felidi", "leone");
$animale_2 = new Zoo("mammiferi", "ursidae", "orso bruno");
$animale_3 = new Zoo("pesci", "salmonidae", "trota");
$animale_4 = new Zoo("pesci", "scombridae", "tonno");
$animale_5 = new Zoo("mammifero", "giraffidae", "giraffa");
$animale_6 = new Zoo("insetto", "ditteri", "zanzara");
$animale_7 = new Zoo("insetto", "pentatomidae", "cimice");
$animale_8 = new Zoo("mammifero", "elephantidae", "elefante");
$animale_9 = new Zoo("rettili", "elapidae", "cobra");
$animale_10 = new Zoo("mammiferi", "canidi", "lupo");


print_r($animale_1);
print_r($animale_2);
print_r($animale_3);
print_r($animale_4);
print_r($animale_5);
print_r($animale_6);
print_r($animale_7);
print_r($animale_8);
print_r($animale_9);
print_r($animale_10);


$animale_1->saluto();
$animale_2->saluto();
$animale_3->saluto();
$animale_4->saluto();
$animale_5->saluto();
$animale_6->saluto();
$animale_7->saluto();
$animale_8->saluto();
$animale_9->saluto();
$animale_10->saluto();

class Carnivori extends Zoo {
  //attributi
  public $carnivori;

  //costruttore
  public function __construct($family, $species, $type, $carnivorous) {
    parent::__construct($family, $species, $type);
      $this -> carnivori = $carnivorous;
  }

  //metodi
  public function hunger() {
    echo "Io sono un $this->tipo e sono un animale carnivoro\n";
  }
  
}

$animale_1 = new Carnivori("mammiferi", "felidi", "leone", "sì");
print_r($animale_1);
$animale_1->hunger();

$animale_2 = new Carnivori("mammiferi", "ursidae", "orso bruno", "sì");
print_r($animale_2);
$animale_2->hunger();

$animale_9 = new Carnivori("rettile", "ursidae", "cobra", "sì");
print_r($animale_9);
$animale_9->hunger();

$animale_10 = new Carnivori("mammiferi", "canidi", "lupo", "sì");
print_r($animale_10);
$animale_10->hunger();

class Erbivori extends Zoo {
  //attributi
  public $erbivori;

  //costruttore
  public function __construct($family, $species, $type, $erbivorous) {
    parent::__construct($family, $species, $type);
      $this -> erbivori = $erbivorous;
  }

  //metodi
  public function vegetarian_hunger() {
    echo "Io sono un $this->tipo e sono un animale erbivoro\n";
  }
  
}

$animale_3 = new Erbivori("pesci", "salmonidae", "trota", "sì");
print_r($animale_3);
$animale_3->vegetarian_hunger();

$animale_4 = new Erbivori("pesci", "scombridae", "tonno", "sì");
print_r($animale_4);
$animale_4->vegetarian_hunger();

$animale_5 = new Erbivori("mammifero", "giraffidae", "giraffa", "sì");
print_r($animale_5);
$animale_5->vegetarian_hunger();

$animale_6 = new Erbivori("insetto", "ditteri", "zanzara", "sì");
print_r($animale_6);
$animale_6->vegetarian_hunger();

$animale_7 = new Erbivori("insetto", "pentatomidae", "zanzara", "sì");
print_r($animale_7);
$animale_7->vegetarian_hunger();

$animale_8 = new Erbivori("mammifero", "elephantidae", "elefante", "sì");
print_r($animale_8);
$animale_8->vegetarian_hunger();

