<?php

// $marta = [
//   "name" => "Marta",
//   "surname" => "Mischiatti",
//   "age" => 29,
//   "media" => 8.5
// ];

// $roberto = [
//   "name" => "Roberto",
//   "surname" => "Sasso",
//   "age" => 26,
//   "subject" => "PHP"
// ];

// $giorgio = [
//   "name" => "Giorgio",
//   "surname" => "Vasto",
//   "age" => 31,
//   "media" => 9.5

// ];

// print_r($marta);
// print_r($roberto);
// print_r($giorgio);

class Person {
  //attributi generali 
  public $name;
  public $surname;
  public $age;

  //attributi statici -- sono chiamati direttamente sulla classe, non sull'oggetto
  public static $count = 0;

  //costruttore
  public function __construct($nome, $cognome, $età) //Parametri formali i cui valori reali saranno quelli degli attributi degli oggetti che andrò a creare
  {
     // $this(riferimento alla classe in cui sono) =>  $parametro reale (attributi che ho indicato) = parametro formale (inizializzato senza dollaro) 
    $this -> name = $nome;
    $this -> surname = $cognome;
    $this -> age = $età;
    self::$count++; //inidico al costruttore di prendere l'attributo stati(::) dalla classe(self)
  }

  //metodi -- comportamenti che voglio dare a tutti gli oggetti della classe 
  public function presentati() {
    echo "Ciao sono $this->surname, $this->name e ho $this->age anni\n";
  }

  public static function contatore() { //metodo statico
    echo self::$count . "\n";
  }

}

echo Person::$count . "\n"; //invocare un attributo statico
Person::contatore(); //invocare un metodo statico

$roberto = new Person("roberto", "sasso", "26"); //Instanziare un oggetto all'interno della classe -- $roberto e $marta sono istanze della classe Person
print_r($roberto);
echo Person::$count . "\n"; //invocare un attributo statico
Person::contatore(); //invocare un metodo statico
$roberto->presentati(); //invocare un metodo


//accedere a un attributo
print_r($roberto -> name . "\n");

$marta = new Person("marta", "mischiatti", "29");
print_r($marta);
$marta->presentati(); //invocare un metodo
echo Person::$count . "\n"; //invocare un attributo statico
Person::contatore(); //invocare un metodo statico

//Specializzare una classe 
class Student extends Person {
  //attributi
  public $media;

  //costruttore
  public function __construct($nome, $cognome, $età, $media) //parametri formali
  {
    parent::__construct($nome, $cognome, $età);//parent:: metodo statico per comunicare con il parent
    $this -> media = $media;

  }

  //metodi
  public function media (){
    echo "La mia media è $this->media\n";
  }
}

$marta = new Student("marta", "mischiatti", 29, 9);
print_r($marta);
$marta->media();

class Teacher extends Person {
  //attributi
  public $subject;
  //costruttore
  public function __construct($nome, $cognome, $età, $materia) {
    parent::__construct($nome, $cognome, $età);//parent:: metodo statico per comunicare con il parent
    $this -> subject = $materia;
  }
  //metodo
  public function subject(){
    echo "Le materie che insegno sono:\n";
    foreach($this->subject as $lezioni) {
      echo $lezioni . "\n";
    }
  }
}

$roberto = new Teacher("roberto", "sasso", 26, ["PHP", "Laravel", "MySql"]);
print_r($roberto);
$roberto->subject();





