<?php
echo "\n";

abstract class ArtoDx {
  abstract public function attack();
}

abstract class ArtoSx {
  abstract public function defence();
}

abstract class ArtiInf {
  abstract public function move();
}

class PugnoVolante extends ArtoDx {
  public function attack(){
    echo "Ti colpisco da casa sul divano\n";
  }
}

class Trivella extends ArtoDx {
  public function attack(){
    echo "Ti buco da sotto\n";
  }
}

class MiniMe extends ArtoDx {
  public function attack(){
    echo "Non ti mettere contro la gang\n";
  }
}

class Scudo extends ArtoSx {
  public function defence(){
    echo "Non mi hai fatto niente faccia di serpente\n";
  }
}

class BarrieraFotonica extends ArtoSx {
  public function defence(){
    echo "Se provi ad attaccarmi ti sciolgo\n";
  }
}

class MantelloInvisibile extends ArtoSx {
  public function defence(){
    echo "Prima mi devi trovare\n";
  }
}

class Cavallo extends ArtiInf {
  public function move(){
    echo "Cavalco verso il sole\n";
  }
}

class Volo extends ArtiInf {
  public function move(){
    echo "Verso l'infinito e oltre\n";
  }
}

class Gambe extends ArtiInf {
  public function move(){
    echo "Mi tocca andare a piedi\n";
  }
}


class JeegRobot{
  public $arto_dx;
  public $arto_sx;
  public $arti_inferiori;

  public function __construct(Artodx $arma, Artosx $difesa, ArtiInf $movimento)
  {
    $this->arto_dx = $arma;
    $this->arto_sx = $difesa;
    $this->arti_inferiori = $movimento;
  }

  public function AttaccoDestro()
  {
    $this->arto_dx->attack();
  }

  public function Difesainistra()
  {
    $this->arto_sx->defence();
  }

  public function Gambe()
  {
    $this->arti_inferiori->move();
  }
}

$jeeg1 = new JeegRobot(new MiniMe, new Scudo, new Cavallo);
print_r($jeeg1);

$jeeg1->AttaccoDestro();
$jeeg1->Difesainistra();
$jeeg1->Gambe();

