<?php
echo "\n";

abstract class ParteAnteriore {
  abstract public function attack();
}

abstract class PartePosteriore {
  abstract public function move();
}

class BatMitragliatrice extends ParteAnteriore {
  public function attack(){
    echo "Ti buco come un colabrodo\n";
  } 
}

class BatBombeFumo extends ParteAnteriore {
  public function attack(){
    echo "Affumicati\n";
  } 
}

class BatLanciaRazzi extends ParteAnteriore {
  public function attack(){
    echo "Ti esplodo\n";
  } 
}

class BatRuota extends PartePosteriore {
  public function move(){
    echo "vai piano che la benzina costa\n";
  } 
}

class BatRampino extends PartePosteriore {
  public function move(){
    echo "arrampica\n";
  } 
}

class BatVola extends PartePosteriore {
  public function move(){
    echo "vado in vacanza\n";
  } 
}

class BatMobile {
  public $attack;
  public $move;

  public function __construct(ParteAnteriore $arma, PartePosteriore $movimento)//dependancy injection
  {
    $this -> attack = $arma;
    $this -> move = $movimento;

  }

  public function attacca(){
    $this->attack->attack();
  }
  
  public function movimento(){
    $this->move->move();
  }
}

//$batmobile1 = new BatMobile("missili", "ruota"); -- segnala errore perchè come parametro si aspetta quelli delle classi astratte
$batMobile = new BatMobile(new BatLanciaRazzi, new BatVola); // composition - compongo l'oggetto con altri oggetti
print_r($batMobile);
$batMobile->attacca();
$batMobile->movimento();
echo "\n";

$batMobile1 = new BatMobile(new BatBombeFumo, new BatRampino);
print_r($batMobile1);
$batMobile1->attacca();
$batMobile1->movimento();

echo "\n";
print_r($batMobile1->attack);//posso accedere agli oggetti creati

