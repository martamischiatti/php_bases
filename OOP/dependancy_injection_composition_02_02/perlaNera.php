<?php

abstract class Poppa {
  abstract public function move();
}

abstract class Prua {
  abstract public function attack();
}

class Vela extends Poppa {
  public function move()
  {
    echo "navighiamo\n";
  }
}

class Remi extends Poppa {
  public function move()
  {
    echo "voga, voga, voga\n";
  }
}

class Cannoni extends Prua {
  public function attack()
  {
    echo "fuoco!\n";
  }
}

class Arrembaggio extends Prua {
  public function attack()
  {
    echo "rampini!\n";
  }
}

class PerlaNera {
  public $poppa;
  public $prua;

  public function __construct(Poppa $poppa, Prua $prua)
  {
    $this-> poppa = $poppa;
    $this-> prua = $prua;
  }

  public function movimento(){
    $this->poppa->move();
  }
  public function attacco(){
    $this->prua->attack();
  }
}

$perla1 = new PerlaNera(new Vela, new Cannoni);
print_r($perla1);
echo "\n";
$perla1->movimento();
$perla1->attacco();
