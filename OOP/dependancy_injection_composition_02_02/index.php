<?php

//creazione classe astratta -- è una classe di cui non si possono istanziare oggetti. Serve dare indirizzi di struttura o metodo che verranno poi ereditati dalle classi figlie
abstract class Animal {
  //attributi
  public $eat;
  public $move;
  public $animalCall;

  //costruttore
  public function __construct($cibo, $movimento, $verso) {
    $this -> eat = $cibo;
    $this -> move = $movimento;
    $this -> animalCall = $verso;
  }

  //metodo 
  abstract public function animalCall(); //Il metodo astratto non ha bisogno del corpo, obbliga il figlio a implementare il metodo stesso, che poi sarà specifico per ogni figlio

  abstract public function move();
}


// $animale = new Animal("carne", "con le zampe", "bau"); --> ouput: "Cannot instantiate abstract class Animal" -- non si può istanziare un oggetto da una classe astratta
// print_r($animale);

class Cat extends Animal {

  public $habitat;

  public function __construct($cibo, $movimento, $verso, $habitat)
  {
    parent::__construct($cibo, $movimento, $verso);
    $this -> habitat = $habitat;
  }

  public function animalCall(){
    echo "miagolo perchè ho fame\n";
  }

  public function move(){
    echo "salto sui mobili\n";
  }

  public function miao(){
    echo "miao\n";
  }
}



class Dog extends Animal {
  public function animalCall(){
    echo "abbiamo perchè ho fame\n";
  }

  public function move(){
    echo "scavo tante buche\n";
  }
}

$animale = new Cat("carne", "con le zampe", "miagola", "casa"); //--> ouput: "Cannot instantiate abstract class Animal" -- non si può istanziare un oggetto da una classe astratta
$animale2 = new Dog("carne", "con le zampe", "abbaia");
print_r($animale);
print_r($animale2);
$animale -> animalCall();
$animale2 -> animalCall();
$animale -> move();
$animale2 -> move();

//Trait -- metodo per ottimizzare codice 

trait Hello {
  public function ciao(){
    echo "ciao a tutti\n";
  }
}

class Saluta {
  use Hello;
}

echo "\n";
$mimmo = new Saluta();
$mimmo->ciao();