<?php


/**
 * Minimo 8 caratteri
 * Almeno una maiuscola
 * Almeno un numero
 * Almeno un carattere speciale ["!", "$", "%", "&", "/", "(", ")", "_", "*", "#", "@"]
 * 
 *  Massimo 5 tentativi
 */

 function checkLength($password) {
  if (strlen($password) >= 8 ) {
    return true;
   } else {
    // echo "Devi inserire almeno 8 caratteri\n";
    return false;
   }
 }



 function checkNumber($password) {
  for ($i=0; $i < strlen($password); $i++) { 
    if (is_numeric($password[$i])) {
      return true;
     } 
    }
    // echo "Devi inserire almeno un numero\n";
    return false;
 }


function checkUpper($password) {
  for ($i=0; $i < strlen($password); $i++) { 
    if (ctype_upper($password[$i])) {
      return true;
     }
    }
    // echo "Devi inserire almeno una lettera maiuscola\n";
    return false;
 }



 const SPECIAL_CHAR =  ["!", "$", "%", "&", "/", "(", ")", "_", "*", "#", "@"];
 function checkSpecial($password) {
  for ($i=0; $i < strlen($password); $i++) { 
    if (in_array($password[$i],SPECIAL_CHAR)) {
      return true;
     } 
    }  
    // echo "Devi inserire almeno un carattere speciale\n";
    return false;     
 }


 function checkPassword($password){    
    switch (true) {
      case checkLength($password) && checkUpper($password) && checkNumber($password) && checkSpecial($password):
        echo "Hai inserito correttamente la password\n";
        return true;
        break;

      case checkLength($password) == false:
        echo "Devi inserire almeno 8 caratteri\n";
      
      case checkUpper($password) == false:
        echo "Devi inserire almeno una lettera maiuscola\n";
      
      case checkNumber($password) == false:
        echo "Devi inserire almeno un numero\n";

      case checkSpecial($password) == false:
        echo "Devi inserire almeno un carattere speciale\n";
        return false;  
        default:
        break;
    }
  }

  // function checkAttempts($checkedPassword) {
  //   $attempts = 5;
  //   if ($checkedPassword==true) {
  //     return;
  //   }
  //   while ($checkedPassword == false && $attempts > 0) {
  //     $attempts--; 
  //     echo "Ti restano $attempts tentativi\n";
      // $pwd = readline("Inserisci una password valida: \n");
      // checkPassword($pwd);
  //   }  
  //   if ($attempts == 0) {
  //     echo "Tentativi esauriti\n";
  //   }
  // }

  function checkAttempts() {
    $attempts=5;
    while ($attempts >0) {
      $pwd = readline("Inserisci una password valida: \n");
      $checkedPassword = checkPassword($pwd);
      if ($checkedPassword == true) {
        return;
      } else if ($checkedPassword ==false) {
        $attempts--;
        echo "Ti restano $attempts tentativi\n";
      }
      if ($attempts==0) {
        echo "Tentativi esauriti\n";
        return;
      }
    }
  }

checkAttempts();

// print_r($pwd . "\n");
