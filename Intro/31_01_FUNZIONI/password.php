<?php

//Scrivere un programma che permetta di validare una password insetira dall'utente
/**
 * Deve contenere almeno 8 caratteri
 * Deve avere almeno una maiuscola
 * Deve avere almeno un numero
 * Deve avere almeno un carattere speciale 
 */

 //APPROCCIO PROCEDURALE - istruzione per istruzione, ma non ottimale

 $pwd = readline("Inserisci la password:\n");

 //1- Deve contenere almeno 8 caretteri
 //strlen()
 if(strlen($pwd) >= 8){
  echo "Hai inserito almeno 8 caratteri\n"; 
 } else {
  echo "Devi inserire almeno 8 caratteri\n";
 }

 //2- Deve contenere almeno una maiuscola
 for($i=0; $i < strlen($pwd); $i++){
  if (ctype_upper($pwd[$i])) {
    echo "Hai inserito almeno una maiuscola\n"; 
    break; //interrompo il ciclo quando viene rispettata la regola
  } else {
    echo "Devi inserire almeno una maiuscola\n";
  }  
 }

 //3- Deve contenere almeno un numero 
 for($i=0; $i < strlen($pwd); $i++) {
  if(is_numeric($pwd[$i])){
    echo "Hai inserito almeno un numero\n"; 
    break; //interrompo il ciclo quando viene rispettata la regola
  } else {
    echo "Devi inserire almeno un numero\n";
  }
 }

 //4- Deve contenere almeno un carattere speciale

 $special_chars = ["!", "#", "£", "@", "$"];
 for($i=0; $i < strlen($pwd); $i++){
  if(in_array($pwd[$i], $special_chars)) {
    echo "Hai inserito almeno un carattere speciale\n"; 
    break;
  } else {
    echo "Devi inserire almeno un carattere speciale\n";
  }
 }

 print_r($pwd . "\n");

 //APPROCCIO FUNZIONALE -- trasformo ogni blocco di codice in una funzione

 $pwd_functional = readline("Inserisci la password:\n");

 //1- Deve contenere almeno 8 caretteri
 function check_length($password){ //Devo inserire il parametro formale per poter accedere alla password nella chiamata inserendola come parametro reale
  if(strlen($password) >= 8){
    echo "Hai inserito almeno 8 caratteri\n"; 
    return true;
   } else {
    echo "Devi inserire almeno 8 caratteri\n";
    return false;
   }
 }

 //salvo il risultato della funzione in una variabile di appoggio
//  $first_rule = check_length($pwd_functional);
//  var_dump($first_rule);



 //2- Deve contenere almeno una maiuscola
 function check_upper($password) {
  for($i=0; $i < strlen($password); $i++){
    if (ctype_upper($password[$i])) {
      echo "Hai inserito almeno una maiuscola\n"; 
      return true;
    }
   }  
   echo "Devi inserire almeno una maiuscola\n";
   return false; 
 }

//  $second_rule = check_upper($pwd_functional);
//  var_dump($second_rule);



 //3- Deve contenere almeno un numero 
 function check_number($password){
  for($i=0; $i < strlen($password); $i++) {
    if(is_numeric($password[$i])){
      echo "Hai inserito almeno un numero\n"; 
      return true; //interrompo il ciclo quando viene rispettata la regola
    } 
   }
   echo "Devi inserire almeno un numero\n";
   return false;
 }

//  $third_rule = check_number($pwd_functional);
//  var_dump($third_rule);

//  //4- Deve contenere almeno un carattere speciale
const SPECIALCHAR = ["!", "#", "£", "@", "$"]; //posso definirlo come costante(meglio in questo caso visto che si tratta di un set di valori che non cambierà) oppure come parametro
function check_special($password){
  for($i=0; $i < strlen($password); $i++){
    if(in_array($password[$i], SPECIALCHAR)) {
      echo "Hai inserito almeno un carattere speciale\n"; 
      return true;
    }
   }
   echo "Devi inserire almeno un carattere speciale\n";
   return false;
}

// $fourth_rule = check_special($pwd_functional);
// var_dump($fourth_rule);

//creo un ulteriore funzione per far interagire le altre regole
function check_password($password){
  //creo variabili di appoggio con le funzioni precedenti
  // $first_rule = check_length($password);
  // $second_rule = check_upper($password);
  // $third_rule = check_number($password);
  // $fourth_rule = check_special($password);

  // if($first_rule && $second_rule && $third_rule && $fourth_rule){
  //   echo "Password valida\n";
  //   return true;
  // } else {
  //   echo "Password non valida\n";
  // }

  //Ottimizzazione codice 
  return check_length($password) && check_upper($password) && check_number($password) && check_special($password);
}

check_password($pwd_functional);

print_r($pwd_functional . "\n");
