<?php
echo "\n";


//dichiarare una funzione

function sayHello() {
  //corpo della funzione
  echo "Ciao sono Marta\n";
}

//invocazione funzione
SayHello();

//Parametri
//-formale è un segnaposto
//-reale contiene il valore reale 

//Somma tra numeri
//$somma = 0;
function sum($numero1, $numero2){
  $somma = $numero1 + $numero2;
  echo "La somma di $numero1 e $numero2 è $somma\n";
  // echo "La somma di $numero1 e $numero2 è ($numero1 + $numero2)\n";
  // echo "La somma di $numero1 e $numero2 è ($somma = $numero1 + $numero2)\n";
}

sum(4, 5);

//SCOPE = visibilità

$num1 =  5; //La varianbile con $ ha scope locale 
const NUM1 = 3; //const ha scope globale (come tutte le costanti)

function stamp_primo_approccio() { //tutto ciò che è all'esterno non è visibile dall'esterno e viceversa
  //echo $num1; output: Warning: Undefined variable $num1
  echo NUM1 . "\n";//la constante è visibile
}

stamp_primo_approccio();

function stamp_secondo_approccio($numero) { //inserisco i parametri per poter sfruttare la variabile all'esterno
  echo $numero . "\n";
}

stamp_secondo_approccio($num1);//uso la variabile come parametro reale



$num2 = 5;

function increment($numero){
  $numero++;
}

echo "Il valore del numero prima dell'incremento è: $num2\n"; 
increment($num2);
echo "Il valore del numero dopo l'incremento è: $num2\n"; //non si ottiene l'incremento perchè nelle funzioni il passaggio è per valore, non per riferimento, viene usata una copia del valore 

function increment2($numero){
  $numero++;
  echo "Il valore del numero dopo l'incremento è: $numero\n";
}

increment2($num2);

function increment_riferimento(&$numero){// inserendo & si imposta il passaggio per riferimento, accedo e scrivo sulla locazione di memoria -- N.B. raramente utilizzato, usato soprattutto nelle built-in function in quanto si manipola il dato reale
  $numero++;
  
}

increment_riferimento($num2);
echo "Il valore del numero dopo l'incremento(riferimento) è: $num2\n";

//impostare un valore di default

$num_default1 = 6;
$num_default2 = 4;

function sum_default($numero1, $numero2, $numero3 = 1){
  echo $numero1 + $numero2 + $numero3 . "\n";
}

sum_default($num_default1,$num_default2);
sum_default($num_default1, $num_default2, 3);

//Splat operator ...
//Può creare un array, o romperlo

$num_splat1 = 10;
$num_splat2 = 5;

function sum_splat(...$numbers){ //con lo splat operator il parametro sarà un array
  print_r($numbers);
  $result = array_reduce($numbers, function($accumulator, $number){ //Funzione senza nome = funzione anonima
  return $accumulator + $number;
  });

  print_r($result);
}

sum_splat($num_splat1, $num_splat2, 3, 14, 4) . "\n";
