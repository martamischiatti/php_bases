<?php
//ESERCIZIO 1
//Dato un array di numeri, scrivere un programma che mi permetta di avere la media SOLO dei numeri pari contenuti all’interno dell’array
echo "Esercizio 1\n";

$numbers = [44, 14, 55, 78, 17, 23];
print_r($numbers);

$pair_numbers = [];
$somma = 0;

foreach ($numbers as $number){
  if($number % 2 == 0){
    $pair_numbers[] = $number;   
  }
}

print_r($pair_numbers);
$somma = array_sum($pair_numbers);
print_r("La somma dei numeri pari è $somma\n");
$count_pair = count($pair_numbers);
$average = $somma/$count_pair;
print_r("La media è $average\n");

//ESERCIZIO 2
// Dato un array di utenti con nome, cognome e genere, per ogni utente stampare “Buongiorno Sig. Nome Cognome” o “Buongiorno Sig.ra Nome Cognome” a seconda del genere
echo "\n";
echo "Esercizio 2";

$users = [
  ["Nicola", "Menonna", "m"],
  ["Andrea", "Mininni", "m"],
  ["Paola", "Pichierri", "f"],
  ["Angela", "Aruanno", "f"],
  ["Roberto", "Sasso", "m"]
];

print_r($users);

foreach($users as $user) {
  if($user[2] == "m"){
    echo "Buongiorno signor $user[0] $user[1]\n";
  } else {
    echo "Buongiorno signora $user[0] $user[1]\n";
  }
};

//Esercizio 3
// Scrivere un programma che stampi in console tutti i numeri da uno a cento. Se il numero è multiplo di 3, stampare “PHP” al posto del numero; se multiplo di 5 deve stampare “JAVASCRIPT”; se multifplo di 3 e 5 (15) deve stampare “HACKADEMY64".

echo "\n";
echo "Esercizio 3\n";

for($i=1; $i <= 100; $i++) {
  if($i % 15 == 0){
    echo "HACKADEMY64\n";
  } else if ($i % 3 == 0){
    echo "PHP\n";
  } else if ($i % 5 == 0){
    echo "JAVASCRIPT\n";
  } else {
    echo $i . "\n";
  }
}

echo "Esercizio 3 - Switch\n";

for($i=1; $i <= 100; $i++) {

  switch ($i) {
    case $i % 15 == 0:
      echo "HACKADEMY64\n";
      break;

    case $i % 3 == 0:
      echo "PHP\n";
      break;
    
    case $i % 5 == 0:
      echo "JAVASCRIPT\n";
      break;
    
    default:
      echo $i . "\n";
      break;
  }
    
}


