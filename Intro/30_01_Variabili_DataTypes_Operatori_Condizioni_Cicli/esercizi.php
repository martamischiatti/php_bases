<?php

//Dato un array di numeri, scrivere un programma che mi permetta di avere la media SOLO dei numeri pari contenuti all’interno dell’array

echo "Esercizio 1" . "\n";

$numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
$even_numbers = [];
$sum = 0;

foreach($numbers as $number) {
  if ($number % 2 == 0) {
    $even_numbers[] = $number;
    foreach($even_numbers as $even_number) {
      $sum += $even_number;
    }
  }
};    

echo "I numeri pari sono: ";
print_r($even_numbers);
echo "La somma dei numeri pari è " . "$sum" . "\n";
$average = $sum / count($even_numbers);
echo "La media dei numeri pari è: " . "$average" . "\n";

echo "\n";
echo "Esercizio 2" . "\n";

// - Dato un array di utenti con nome, cognome e genere, per ogni utente stampare “Buongiorno Sig. Nome Cognome” o “Buongiorno Sig.ra Nome Cognome” a seconda del genere
// $users = [
//   ["Nicola", "Menonna", 'm'],
//   ["Andrea", "Mininni", 'm'],
//   ["Paola", "Pichierri", 'f'],
//   ["Angela", "Aruanno", 'f'],
//   ["Roberto", "Sasso", 'm']
// ];

$users = [
  ["Nicola", "Menonna", 'm'],
  ["Andrea", "Mininni", 'm'],
  ["Paola", "Pichierri", 'f'],
  ["Angela", "Aruanno", 'f'],
  ["Roberto", "Sasso", 'm']
];


foreach($users as $user) {
  if ($user[2] == "m") {
    echo "Buongiorno sig. " . "$user[0], $user[1]" . "\n";
  } else {
    echo "Buongiorno sig.ra " . "$user[0], $user[1]" . "\n";
  }
};

echo "\n";
echo "Esercizio 3" . "\n";

// Scrivere un programma che stampi in console tutti i numeri da uno a cento. Se il numero è multiplo di 3, stampare “PHP” al posto del numero; se multiplo di 5 deve stampare “JAVASCRIPT”; se multifplo di 3 e 5 (15) deve stampare “HACKADEMY64".


for ($i = 1; $i <= 100; $i++) { 
  if ($i % 15 == 0) {
    echo "HACKADEMY64" . "\n";
  } else if ($i % 3 == 0) {
    echo "PHP" . "\n";
  } else if ($i % 5 == 0) {
    echo "JAVASCRIPT" . "\n";
  } else {
    echo "$i" . "\n";
  }
};

